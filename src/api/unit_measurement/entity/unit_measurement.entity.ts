import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('unit_measurement')
export class UnitMeasurementEntity {
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Column({
    type: 'varchar',
    unique: true,
  })
  readonly name: string;

  @Column({ type: 'varchar' })
  readonly label: string;

  @Column({ type: 'boolean' })
  readonly state: boolean;

  @Column({ type: 'timestamp' })
  readonly createAt: Date;

  @Column({ type: 'timestamp' })
  readonly updateAt: Date;

  constructor(
    id: string,
    name: string,
    label: string,
    state: boolean,
    createAt: Date,
    updateAt: Date,
  ) {
    this.id = id;
    this.name = name;
    this.label = label;
    this.state = state;
    this.createAt = createAt;
    this.updateAt = updateAt;
    console.log('Creo UnitMeasurement Entity para ' + this.id);
  }
}
