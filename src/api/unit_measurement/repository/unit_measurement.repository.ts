import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, In, Repository } from 'typeorm';
import { UnitMeasurementEntity } from '../entity/unit_measurement.entity';
import { UnitMeasurementMapper } from '../mapper/unit_measurement.mapper';
import { UnitMeasurementDTO } from '../dto/unit_measurement.dto';

@Injectable()
export class UnitMeasurementRepository {
  constructor(
    @InjectRepository(UnitMeasurementEntity)
    private unitMeasurementRepository: Repository<UnitMeasurementEntity>,
    private mapper: UnitMeasurementMapper,
  ) {}

  getAllUnitMeasurements(): Promise<UnitMeasurementEntity[]> {
    return this.unitMeasurementRepository.find();
  }

  getUnitMeasurementById(id: string): Promise<UnitMeasurementEntity> {
    return this.unitMeasurementRepository.findOne({
      where: { id },
    });
  }

  newUnitMeasurement(
    unitMeasurementDTO: UnitMeasurementDTO,
  ): Promise<UnitMeasurementEntity> {
    const newUnitMeasurement = this.mapper.dtoToEntity(unitMeasurementDTO);
    return this.unitMeasurementRepository.save(newUnitMeasurement);
  }

  async updateUnitMeasurement(
    id: string,
    unitMeasurementDTO: UnitMeasurementDTO,
  ): Promise<UnitMeasurementEntity> {
    unitMeasurementDTO.id = id;
    const updateUnitMeasurement = this.mapper.dtoToEntity(unitMeasurementDTO);
    await this.unitMeasurementRepository.update(id, updateUnitMeasurement);
    return this.unitMeasurementRepository.findOne({ where: { id } });
  }

  deleteUnitMeasurement(id: string): Promise<DeleteResult> {
    return this.unitMeasurementRepository.delete(id);
  }
}
