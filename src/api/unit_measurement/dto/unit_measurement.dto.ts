import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsOptional, IsString } from 'class-validator';

export class UnitMeasurementDTO {
  @ApiProperty()
  @IsOptional()
  @IsString()
  id: string;

  @ApiProperty()
  @IsString()
  readonly name: string;

  @ApiProperty()
  @IsString()
  readonly label: string;

  @ApiProperty()
  @IsBoolean()
  readonly state: boolean;

  @ApiProperty()
  @IsOptional()
  @Type(() => Date)
  @IsDate()
  createAt: Date;

  @ApiProperty()
  @IsOptional()
  @Type(() => Date)
  @IsDate()
  updateAt: Date;

  constructor(
    id: string,
    name: string,
    label: string,
    state: boolean,
    createAt: Date,
    updateAt: Date,
  ) {
    this.id = id;
    this.name = name;
    this.label = label;
    this.state = state;
    this.createAt = createAt;
    this.updateAt = updateAt;
  }
}
