import { Module } from '@nestjs/common';
import { UnitMeasurementController } from './unit_measurement.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UnitMeasurementEntity } from './entity/unit_measurement.entity';
import { UnitMeasurementMapper } from './mapper/unit_measurement.mapper';
import { UnitMeasurementRepository } from './repository/unit_measurement.repository';
import { UnitMeasurementService } from './unit_measurement.service';

@Module({
  imports: [TypeOrmModule.forFeature([UnitMeasurementEntity])],
  controllers: [UnitMeasurementController],
  providers: [
    UnitMeasurementMapper,
    UnitMeasurementRepository,
    UnitMeasurementService,
  ],
})
export class UnitMeasurementModule {}
