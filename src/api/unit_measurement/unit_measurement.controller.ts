import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UnitMeasurementDTO } from './dto/unit_measurement.dto';
import { UnitMeasurementService } from './unit_measurement.service';

@Controller('unit-measurement')
export class UnitMeasurementController {
  constructor(private unitMeasurementService: UnitMeasurementService) {}

  @Get()
  async getAllUnitMeasurement(): Promise<UnitMeasurementDTO[]> {
    return await this.unitMeasurementService.getAllUnitMeasurements();
  }

  @Get(':id')
  async getUnitMeasurementById(
    @Param('id') id: string,
  ): Promise<UnitMeasurementDTO> {
    return await this.unitMeasurementService.getUnitMeasurementById(id);
  }

  @Post()
  async newUnitMeasurement(
    @Body() unitMeasurement: UnitMeasurementDTO,
  ): Promise<UnitMeasurementDTO> {
    unitMeasurement.createAt = new Date();
    unitMeasurement.updateAt = unitMeasurement.createAt;

    return await this.unitMeasurementService.newUnitMeasurement(
      unitMeasurement,
    );
  }

  @Put(':id')
  async updateUnitMeasurement(
    @Param('id') id: string,
    @Body() unitMeasurement: UnitMeasurementDTO,
  ): Promise<UnitMeasurementDTO> {
    unitMeasurement.updateAt = new Date();
    return await this.unitMeasurementService.updateUnitMeasurement(
      id,
      unitMeasurement,
    );
  }

  @Delete('/:id')
  async deleteUnitMeasurement(@Param('id') id: string): Promise<void> {
    return await this.unitMeasurementService.deleteUnitMeasurement(id);
  }
}
