import { Injectable } from '@nestjs/common';
import { UnitMeasurementRepository } from './repository/unit_measurement.repository';
import { UnitMeasurementMapper } from './mapper/unit_measurement.mapper';
import { UnitMeasurementDTO } from './dto/unit_measurement.dto';
import { UnitMeasurementEntity } from './entity/unit_measurement.entity';

@Injectable()
export class UnitMeasurementService {
  constructor(
    private unitMeasurementRepository: UnitMeasurementRepository,
    private mapper: UnitMeasurementMapper,
  ) {}

  async getAllUnitMeasurements(): Promise<UnitMeasurementDTO[]> {
    const unitMeasurements: UnitMeasurementEntity[] =
      await this.unitMeasurementRepository.getAllUnitMeasurements();
    return unitMeasurements.map((unitMeasurement) =>
      this.mapper.entityToDto(unitMeasurement),
    );
  }

  async getUnitMeasurementById(id: string): Promise<UnitMeasurementDTO> {
    const unitMeasurement: UnitMeasurementEntity =
      await this.unitMeasurementRepository.getUnitMeasurementById(id);
    return this.mapper.entityToDto(unitMeasurement);
  }

  async newUnitMeasurement(
    unitMeasurementDTO: UnitMeasurementDTO,
  ): Promise<UnitMeasurementDTO> {
    const newUnitMeasurementDTO: UnitMeasurementEntity =
      await this.unitMeasurementRepository.newUnitMeasurement(
        unitMeasurementDTO,
      );
    return this.mapper.entityToDto(newUnitMeasurementDTO);
  }

  async updateUnitMeasurement(
    id: string,
    unitMeasurementDTO: UnitMeasurementDTO,
  ): Promise<UnitMeasurementDTO> {
    const updateUnitMeasurement =
      await this.unitMeasurementRepository.updateUnitMeasurement(
        id,
        unitMeasurementDTO,
      );
    return this.mapper.entityToDto(updateUnitMeasurement);
  }

  async deleteUnitMeasurement(id: string): Promise<void> {
    await this.unitMeasurementRepository.deleteUnitMeasurement(id);
  }
}
