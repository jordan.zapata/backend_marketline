import { Injectable } from '@nestjs/common';
import { UnitMeasurementEntity } from '../entity/unit_measurement.entity';
import { UnitMeasurementDTO } from '../dto/unit_measurement.dto';

@Injectable()
export class UnitMeasurementMapper {
  dtoToEntity(unitMeasurementDTO: UnitMeasurementDTO): UnitMeasurementEntity {
    return new UnitMeasurementEntity(
      unitMeasurementDTO.id,
      unitMeasurementDTO.name,
      unitMeasurementDTO.label,
      unitMeasurementDTO.state,
      unitMeasurementDTO.createAt,
      unitMeasurementDTO.updateAt,
    );
  }

  entityToDto(
    unitMeasurementEntity: UnitMeasurementEntity,
  ): UnitMeasurementDTO {
    return new UnitMeasurementDTO(
      unitMeasurementEntity.id,
      unitMeasurementEntity.name,
      unitMeasurementEntity.label,
      unitMeasurementEntity.state,
      unitMeasurementEntity.createAt,
      unitMeasurementEntity.updateAt,
    );
  }
}
